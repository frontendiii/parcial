import React from 'react'
import { useState } from 'react'
import Card from './Card'

const Form = () => {
    const [input1, setInput1] = useState('');
    const [input2, setInput2] = useState('');
    const [error, setError] = useState(false);
    const [showInfo, setShowInfo] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (input1.trim().length < 3 || input2.length < 6) {
            setError(true);
            setShowInfo(false);
        } else {
            setError(false);
            setShowInfo(true);
        }
    }

  return (
    <React.Fragment>
        <div style={{ maxWidth: '300px', margin: '0 auto' }}>
            <form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column' }}>
                <input
                    type='text'
                    placeholder='Ingresa tu nombre'
                    value={input1}
                    onChange={e => setInput1(e.target.value)}
                    style={{ padding: '8px' }}
                />
                <br />
                <input
                    type='text'
                    placeholder='Ingresa tu libro favorito'
                    value={input2}
                    onChange={ e => setInput2(e.target.value)}
                    style={{ padding: '8px' }}
                />
                <button type='submit' style={{ margin: '16px 0', padding: '8px 15px', backgroundColor: '#007bff', color: '#fff', border: 'none', borderRadius: '4px', cursor: 'pointer', }}>Enviar</button>
            </form>

            {error && <p style={{ color: 'red', marginTop: '10px' }}>Por favor chequea que la información sea correcta</p>}

            {showInfo && (
                <Card nombre={input1} libro={input2}/>
            )}
        </div>
    </React.Fragment>
  )
}

export default Form;
