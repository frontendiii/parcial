import React from 'react'

const Card = (props) => {
  const { nombre, libro } = props;
  return (
    <React.Fragment>
    <div style={{ border: '1px solid #ccc', padding: '10px', marginTop: '20px' }}>
      <h3>Información ingresada: </h3>
      <h4>Hola {nombre}!</h4>
      <p>Sabemos que tu libro favorito es: {libro}.</p>
    </div>
    </React.Fragment>
  )
}

export default Card;
